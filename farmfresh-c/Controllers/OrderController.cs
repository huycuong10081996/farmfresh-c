﻿using System;
using farmfresh_c.DTO;
using farmfresh_c.Helpers;
using farmfresh_c.Models;
using Microsoft.AspNetCore.Mvc;
using farmfresh_c.Services;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace farmfresh_c.Controllers {

    [ApiController]
    [Route("api/v1/order")]
    public class OrderController : Controller {
        private IOrderService _orderService;

        public OrderController(IOrderService _orderService) {
            this._orderService = _orderService;
        }

        [Authorize]
        [HttpGet("all/{userId:int}")]
        public ActionResult<OrderDTO> GetByUserId(int userId, int? page = 1, int? limit = 20) {
            int skip = (int)((page - 1) * limit);

            var result = this._orderService.GetOrderByUserId(userId, skip, (int)limit);

            int count = this._orderService.CountOrderByUserId(userId);

            OrderDTO orderDTO = new OrderDTO(result, count, limit, skip);

            return new ActionResult<OrderDTO>(orderDTO);
        }

        [Authorize]
        [HttpPost("checkout")]
        public async Task<ActionResult> Create([FromBody] OrderCreateDto orderCreateDto) {
            int newId = this._orderService.CreateNewId();

            Collection<OrderDetail> orderDetails = _orderService.ToCollection(orderCreateDto.products, newId);

            Order order = new Order {
                id = newId,
                district = orderCreateDto.district,
                address = orderCreateDto.address,
                province = orderCreateDto.province,
                firstName = orderCreateDto.firstName,
                lastName = orderCreateDto.lastName,
                phone = orderCreateDto.phone,
                deliveryDate = orderCreateDto.deliveryDate,
                status = 2,
                totalMoney = (double)orderCreateDto.totalAmount,
                userId = (int)orderCreateDto.userId,
                createdAt = DateTime.UtcNow,
                updatedAt = DateTime.UtcNow
            };

            Order orderSaved = await _orderService.CreateOrder(order);

            foreach (OrderDetail o in orderDetails) {
                o.orderId = orderSaved.id;
                OrderDetail orderDetail = await _orderService.CreateOrderDetail(o);
            }

            return Ok(new { success = true, order = orderSaved });

        }



    }
}
