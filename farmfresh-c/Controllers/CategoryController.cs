﻿using farmfresh_c.DTO;
using farmfresh_c.Helpers;
using farmfresh_c.Models;
using farmfresh_c.Services;
using Microsoft.AspNetCore.Mvc;

namespace farmfresh_c.Controllers {
    [ApiController]
    [Route("api/v1/category")]
    public class CategoryController : Controller {
        private ICategoryService _categoryService;
        public CategoryController(ICategoryService _categoryService) {
            this._categoryService = _categoryService;
        }

        [HttpGet]
        public ActionResult<CategoryDTO> Get(int? page = 1, int? limit = 20) {
            int skip = (int)((page - 1) * limit);

            var result = this._categoryService.GetCategoriesForClient(skip, (int)limit);

            int count = this._categoryService.CountCategoryForClient();

            CategoryDTO categoryDTO = new CategoryDTO(result, count, limit, skip);

            return new ActionResult<CategoryDTO>(categoryDTO);
        }

        [AdminRole]
        [HttpGet("detail/{id:int}")]
        public Category GetById(int id) {
            Category category = this._categoryService.GetById(id);
            return category;
        }

    }
}
