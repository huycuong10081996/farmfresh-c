﻿using System;

using Microsoft.AspNetCore.Mvc;
using farmfresh_c.Models;
using farmfresh_c.DTO;
using System.Threading.Tasks;
using farmfresh_c.Services;
using farmfresh_c.Helpers;

namespace farmfresh_c.Controllers {
    [ApiController]
    [Route("api/v1/blog")]
    public class BlogController : Controller {
        private IBlogService _blogService;
        public BlogController(IBlogService blogService) {
            this._blogService = blogService;
        }

        [HttpGet]
        public ActionResult<BlogDTO> Get(int? page = 1, int? limit = 20) {
            int count = this._blogService.CountBlog();

            int skip = (int)((page - 1) * limit);

            var blog = this._blogService.GetAllBlogs(skip, (int)limit);

            BlogDTO blogDTO = new BlogDTO(blog, count, limit, skip);

            return new ActionResult<BlogDTO>(blogDTO);
        }

        [HttpGet("{slug}")]
        public Blog GetBySlug(string slug) {
            Blog blog = this._blogService.GetBySlug(slug);

            return blog;
        }


    }
}

