﻿using System;
using System.Threading.Tasks;
using farmfresh_c.DTO;
using farmfresh_c.Helpers;
using farmfresh_c.Models;
using farmfresh_c.Services;
using Microsoft.AspNetCore.Mvc;

namespace farmfresh_c.Controllers {
    [ApiController]
    [Route("api/v1/product")]
    public class ProductController : Controller {
        private IProductService _productService;

        public ProductController(IProductService _productService) {
            this._productService = _productService;
        }

        [HttpGet]
        public ActionResult<ProductDTO> GetAllProduct(int? page = 1, int? limit = 20) {
            var result = this._productService.GetAllProductsForClient(0, (int)limit);

            int count = this._productService.CountProductForClient();

            ProductDTO productDTO = new ProductDTO(result, count, limit, 0);

            return new ActionResult<ProductDTO>(productDTO);
        }

        [HttpGet("detail/{id:int}")]
        public Product GetById(int id) {
            Product product = this._productService.GetById(id);

            return product;
        }

        [HttpGet("related/{id:int}")]
        public ActionResult<ProductDTO> GetRelatedProduct(int id, int? page = 1, int? limit = 20) {
            int skip = (int)((page - 1) * limit);

            var result = this._productService.GetRelatedProducts(id, skip, (int)limit);
            int count = this._productService.CountRelatedProduct(id);

            ProductDTO productDTO = new ProductDTO(result, count, limit, 0);

            return new ActionResult<ProductDTO>(productDTO);
        }

        [HttpGet("category/{name}")]
        public ActionResult<ProductDTO> GetProductByCategory(string name, string sort, int? page = 1, int? limit = 20) {
            int skip = (int)((page - 1) * limit);

            var result = this._productService.GetProductsByCategory(skip, (int)limit, name);

            result = this._productService.HandleSort(result, sort);

            int count = this._productService.CountProductByCategory(name);

            ProductDTO productDTO = new ProductDTO(result, count, limit, 0);

            return new ActionResult<ProductDTO>(productDTO);
        }

        [Authorize]
        [HttpPost("rating/{productId:int}")]
        public async Task<ActionResult> Create([FromBody] RatingCreateDTO ratingCreateDTO, int? productId) {
            int newId = this._productService.CreateNewRatingId();

            Rating rating1 = new Rating {
                id = newId,
                ratingFor = "PRODUCT",
                description = ratingCreateDTO.description,
                rating = (int)ratingCreateDTO.rating,
                productId = ratingCreateDTO.productId,
                userId = (int)ratingCreateDTO.userId,
                createdAt = DateTime.UtcNow,
                updatedAt = DateTime.UtcNow
            };

            Rating rating2 = await this._productService.CreateRating(rating1, newId);

            return Ok(new { success = true, data = rating2 });
        }


    }
}
