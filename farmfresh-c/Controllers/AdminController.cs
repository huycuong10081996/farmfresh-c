﻿using System;
using System.Threading.Tasks;
using farmfresh_c.DTO;
using farmfresh_c.DTO.user;
using farmfresh_c.Helpers;
using farmfresh_c.Models;
using farmfresh_c.Services;
using Microsoft.AspNetCore.Mvc;

namespace farmfresh_c.Controllers {
    [ApiController]
    [Route("api/v1/admin")]
    public class AdminController : Controller {
        private ICategoryService _categoryService;
        private IBlogService _blogService;
        private IProductService _productService;
        private IUserService _userService;
        private IOrderService _orderService;

        public AdminController(ICategoryService _categoryService, IBlogService _blogService, IProductService _productService, IUserService _userService, IOrderService _orderService) {
            this._categoryService = _categoryService;
            this._blogService = _blogService;
            this._productService = _productService;
            this._userService = _userService;
            this._orderService = _orderService;
        }

        [AdminRole]
        [HttpGet("orders")]
        public ActionResult<OrderDTO> GetAllOrders(int userId, int? page = 1, int? limit = 20) {
            int skip = (int)((page - 1) * limit);

            var result = this._orderService.GetOrders(skip, (int)limit);

            int count = this._orderService.CountOrder();

            OrderDTO orderDTO = new OrderDTO(result, count, limit, skip);

            return new ActionResult<OrderDTO>(orderDTO);
        }

        [AdminRole]
        [HttpGet("orders/detail/{id:int}")]
        public Order GetOrderById(int id) {
            Order order = this._orderService.GetById(id);

            return order;
        }

        [AdminRole]
        [HttpPost("orders/update/{id:int}")]
        public async Task<ActionResult<Order>> UpdateProduct([FromBody] OrderCreateDto orderCreateDto, int id) {
            Order order1 = new Order {
                id = id,
                district = orderCreateDto.district,
                address = orderCreateDto.address,
                province = orderCreateDto.province,
                firstName = orderCreateDto.firstName,
                lastName = orderCreateDto.lastName,
                phone = orderCreateDto.phone,
                deliveryDate = orderCreateDto.deliveryDate,
                status = (int)orderCreateDto.status,
                updatedAt = DateTime.UtcNow
            };

            Order order2 = await this._orderService.UpdateOrder(order1, id);

            return order2;
        }

        [AdminRole]
        [HttpGet("products")]
        public ActionResult<ProductDTO> GetAllProductForAdmin(int? page = 1, int? limit = 20) {
            int skip = (int)((page - 1) * limit);

            var result = this._productService.GetAllProductsForAdmin(skip, (int)limit);

            int count = this._productService.CountProduct();

            ProductDTO productDTO = new ProductDTO(result, count, limit, skip);

            return new ActionResult<ProductDTO>(productDTO);
        }

        [AdminRole]
        [HttpPost("products/create")]
        public async Task<ActionResult> CreateProduct([FromBody] ProductCreateDTO productCreateDTO) {
            int newId = this._productService.CreateNewId();

            Product product1 = new Product {
                id = newId,
                name = productCreateDTO.name,
                imageUrl = productCreateDTO.imageUrl,
                status = productCreateDTO.status,
                type = productCreateDTO.type,
                price = (double)productCreateDTO.price,
                discount = productCreateDTO.discount,
                description = productCreateDTO.description,
                quantity = (int)productCreateDTO.quantity,
                color = productCreateDTO.color,
                categoryId = (int)productCreateDTO.categoryId,
                createdAt = DateTime.UtcNow,
                updatedAt = DateTime.UtcNow
            };

            Product product2 = await this._productService.CreateProduct(product1, newId);

            return Ok(new { success = true, data = product2 });
        }

        [AdminRole]
        [HttpPost("products/update/{id:int}")]
        public async Task<ActionResult> UpdateProduct([FromBody] ProductCreateDTO productCreateDTO, int id) {

            Product product1 = new Product {
                id = id,
                name = productCreateDTO.name,
                imageUrl = productCreateDTO.imageUrl,
                status = productCreateDTO.status,
                type = productCreateDTO.type,
                price = (double)productCreateDTO.price,
                discount = productCreateDTO.discount,
                description = productCreateDTO.description,
                quantity = (int)productCreateDTO.quantity,
                color = productCreateDTO.color,
                categoryId = (int)productCreateDTO.categoryId,
                updatedAt = DateTime.UtcNow
            };

            Product product2 = await this._productService.UpdateProduct(product1, id);

            return Ok(new { success = true, data = product2 });
        }

        [AdminRole]
        [HttpDelete("products/detail/{id:int}")]
        public async Task<ActionResult> DeleteProduct(int id) {

            Product product1 = new Product { id = id };

            bool success = await this._productService.DeleteProduct(product1);

            return Ok(new { success = success, id = id });
        }

        [AdminRole]
        [HttpGet("categories")]
        public ActionResult<CategoryDTO> GetAllCategoryForAdmin(int? page = 1, int? limit = 20) {
            int skip = (int)((page - 1) * limit);

            var result = this._categoryService.GetCategoriesForAdmin(skip, (int)limit);

            int count = this._categoryService.CountCategory();

            CategoryDTO categoryDTO = new CategoryDTO(result, count, limit, skip);

            return new ActionResult<CategoryDTO>(categoryDTO);
        }

        [AdminRole]
        [HttpPost("categories/create")]
        public async Task<ActionResult> CreateCategory([FromBody] CategoryCreateDTO categoryCreateDTO) {
            int newId = this._categoryService.CreateNewId();

            Category category1 = new Category {
                id = newId,
                title = categoryCreateDTO.title,
                slug = categoryCreateDTO.slug,
                showToHomePage = (bool)categoryCreateDTO.showToHomePage,
                createdAt = DateTime.UtcNow,
                updatedAt = DateTime.UtcNow
            };

            Category category = await this._categoryService.CreateCategory(category1, newId);

            return Ok(new { success = true, data = category });
        }

        [AdminRole]
        [HttpPost("categories/update/{id:int}")]
        public async Task<ActionResult> UpdateCategory([FromBody] CategoryCreateDTO categoryCreateDTO, int id) {

            Category category1 = new Category {
                id = id,
                title = categoryCreateDTO.title,
                showToHomePage = (bool)categoryCreateDTO.showToHomePage,
                slug = categoryCreateDTO.slug,
                updatedAt = DateTime.UtcNow
            };

            Category category = await this._categoryService.UpdateCategory(category1, id);

            return Ok(new { success = true, data = category });
        }

        [AdminRole]
        [HttpDelete("categories/detail/{id:int}")]
        public async Task<ActionResult> DeleteCategory(int id) {

            Category category = new Category { id = id };

            bool success = await this._categoryService.DeleteCategory(category);

            return Ok(new { success = success, id = id });
        }

        [AdminRole]
        [HttpGet("blogs")]
        public ActionResult<BlogDTO> GetAllBlogsForAdmin(int? page = 1, int? limit = 20) {
            int skip = (int)((page - 1) * limit);

            var result = this._blogService.GetAllBlogs(skip, (int)limit);

            int count = this._blogService.CountBlog();

            BlogDTO blogDTO = new BlogDTO(result, count, limit, skip);

            return new ActionResult<BlogDTO>(blogDTO);
        }

        [AdminRole]
        [HttpGet("accounts")]
        public ActionResult<UserDTO> Get(int? roleId = 3, int? page = 1, int? limit = 20) {
            int skip = (int)((page - 1) * limit);

            var result = this._userService.GetAllUsers((int)roleId, skip, (int)limit);

            int count = this._userService.CountByRole((int)roleId);

            UserDTO userDTO = new UserDTO(result, count, limit, skip);

            return new ActionResult<UserDTO>(userDTO);
        }

        [AdminRole]
        [HttpGet("accounts/detail/{id:int}")]
        public User GetById(int id) {
            User user = this._userService.GetByIdForAdmin(id);
            return user;
        }

        [HttpPost("accounts/register")]
        public async Task<ActionResult<User>> Register([FromBody] UserUpdateForAdminDTO userCreateDTO) {

            User user = _userService.FindUserByEmail(userCreateDTO.email);
            if (user != null) return BadRequest("User already exists");
            User u1 = new User();
            u1.email = userCreateDTO.email;
            u1.password = userCreateDTO.password;
            u1.firstName = userCreateDTO.firstName;
            u1.lastName = userCreateDTO.lastName;
            u1.name = userCreateDTO.name;
            u1.address = userCreateDTO.address;
            u1.district = userCreateDTO.district;
            u1.province = userCreateDTO.province;
            u1.phone = userCreateDTO.phone;
            u1.roleId = userCreateDTO.roleId;
            u1.status = (int)userCreateDTO.status;
            u1.rejectReason = userCreateDTO.rejectReason;
            User userSaved = await _userService.SaveUser(u1, userCreateDTO.roleId);
            return userSaved;
        }

        [AdminRole]
        [HttpPost("accounts/update/{id:int}")]
        public ActionResult<User> UpdateAccount([FromBody] UserUpdateForAdminDTO userCreateDTO) {
            User user = this._userService.FindUserByEmail(userCreateDTO.email);
            if (user == null) return BadRequest(new { success = false, message = "email not found!" });
            this._userService.UpdateInfoForAdmin(userCreateDTO);
            return _userService.GetById(user.id);
        }

        [HttpPost("accounts/login")]
        public ActionResult<AuthenticateResponse> Login([FromBody] UserCreateDTO userCreateDTO) {
            User user = _userService.FindUserByEmailForLogin(userCreateDTO.email);

            if (user == null) return Unauthorized(new { message = "Wrong username or password" });
            if (user.status == 1) return Unauthorized(new { message = "Your account has been locked by admin cause of " + user.rejectReason });
            if (user.roleId == 3) return Unauthorized(new { message = "Permission Denied" });

            if (this._userService.VerifyPassword(user, userCreateDTO.password)) {
                var accessToken = this._userService.generateJwtToken(user, 7);
                var refreshToken = this._userService.generateJwtToken(user, 365);

                Authenticate authenticate = new Authenticate(refreshToken, accessToken);

                _userService.SaveLogin(user.id, accessToken, refreshToken);

                return new AuthenticateResponse(true, authenticate);
            }
            return Unauthorized(new { message = "Wrong username or password" });
        }

        [AdminRole]
        [HttpPost()]
        public async Task<ActionResult> Create([FromBody] BlogCreateDTO blogCreateDTO) {
            int newId = this._blogService.CreateNewId();

            Blog blog1 = new Blog {
                id = newId,
                title = blogCreateDTO.title,
                content = blogCreateDTO.content,
                image = blogCreateDTO.image,
                status = blogCreateDTO.status,
                slug = blogCreateDTO.slug,
                userId = blogCreateDTO.userId,
                createdAt = DateTime.UtcNow,
                updatedAt = DateTime.UtcNow
            };

            Blog blog = await this._blogService.CreateBlog(blog1);

            return Ok(new { blog = blog });

        }

    }
}
