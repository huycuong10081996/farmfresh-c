﻿using farmfresh_c.DTO;
using farmfresh_c.Models;
using Microsoft.AspNetCore.Mvc;
using farmfresh_c.Services;
using System.Threading.Tasks;
using farmfresh_c.Helpers;

namespace farmfresh_c.Controllers {
    [ApiController]
    [Route("api/v1/account")]
    public class UserController : Controller {
        private IUserService _userService;

        public UserController(IUserService userService) {
            this._userService = userService;
        }

        [Authorize]
        [HttpGet("{id:int}")]
        public User GetById(int id) {
            User user = this._userService.GetById(id);

            return user;
        }

        [HttpPost("login")]
        public ActionResult<AuthenticateResponse> Login([FromBody] UserCreateDTO userCreateDTO) {
            User user = _userService.FindUserByEmailForLogin(userCreateDTO.email);
            if (user == null) return Unauthorized(new { message = "Wrong username or password" });
            if (user.status == 1) return Unauthorized(new { message = "Your account has been locked by admin cause of " + user.rejectReason });
            if (this._userService.VerifyPassword(user, userCreateDTO.password)) {
                var accessToken = this._userService.generateJwtToken(user, 7);
                var refreshToken = this._userService.generateJwtToken(user, 365);
                Authenticate authenticate = new Authenticate(refreshToken, accessToken);
                _userService.SaveLogin(user.id, accessToken, refreshToken);
                return new AuthenticateResponse(true, authenticate);
            }
            return Unauthorized(new { message = "Wrong username or password" });
        }

        [HttpPost("info")]
        public User GetUserInfo([FromBody] Authenticate authenticate) {
            User user = this._userService.GetUserByAccessToken(authenticate.accessToken);

            return user;
        }

        [Authorize]
        [HttpPost("update/{id:int}")]
        public ActionResult<User> UpdateAccount([FromBody] UserCreateDTO userCreateDTO) {
            User user = this._userService.FindUserByEmail(userCreateDTO.email);
            if (user == null) return BadRequest(new { success = false, message = "email not found!" });
            this._userService.UpdateInfo(userCreateDTO);
            return _userService.GetById(user.id);
        }

        [HttpPost("register")]
        public async Task<ActionResult<User>> Register([FromBody] UserCreateDTO userCreateDTO) {

            User user = _userService.FindUserByEmail(userCreateDTO.email);
            if (user != null) return BadRequest("User already exists");
            User u1 = new User();
            u1.email = userCreateDTO.email;
            u1.password = userCreateDTO.password;
            u1.firstName = userCreateDTO.firstName;
            u1.lastName = userCreateDTO.lastName;
            u1.name = userCreateDTO.name;
            u1.address = userCreateDTO.address;
            u1.district = userCreateDTO.district;
            u1.province = userCreateDTO.province;
            u1.phone = userCreateDTO.phone;
            User userSaved = await _userService.SaveUser(u1, 3);
            return userSaved;
        }

    }
}
