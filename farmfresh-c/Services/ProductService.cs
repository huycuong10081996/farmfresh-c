﻿using System.Collections.Generic;
using System.Linq;
using farmfresh_c.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System;

namespace farmfresh_c.Services {
    public interface IProductService {
        List<Product> GetAllProductsForClient(int skip, int limit);

        List<Product> GetRelatedProducts(int id, int skip, int limit);

        List<Product> GetAllProductsForAdmin(int skip, int limit);

        List<Product> GetProductsByCategory(int skip, int limit, string slug);

        List<Product> HandleSort(ICollection<Product> products, string sortType);

        Product GetById(int id);

        int CountProductByCategory(string slug);

        int CountProductForClient();

        int CreateNewId();

        int CountProduct();

        int CountRelatedProduct(int id);

        int CreateNewRatingId();

        public Task<Product> CreateProduct(Product product, int newId);

        public Task<Product> UpdateProduct(Product product, int id);

        public Task<Rating> CreateRating(Rating rating, int newId);

        public Task<bool> DeleteProduct(Product product);
    }

    public class ProductService : IProductService {
        public readonly DataContext _db;

        public ProductService(DataContext db) {
            this._db = db;
        }

        public int CountProduct() {
            return _db.Products.Count();
        }

        public int CountProductByCategory(string slug) {
            return _db.Products.Select(p => new Product {
                id = p.id,
                status = p.status,
                category = new Category {
                    id = p.category.id,
                    title = p.category.title,
                    slug = p.category.slug
                },
            }).Where(p => p.category.slug == slug && p.status == 0).Count();
        }

        public int CountProductForClient() {
            return _db.Products.Select(p => new Product { id = p.id, status = p.status })
                 .Where(p => p.status == 0).Count();
        }

        public int CountRelatedProduct(int id) {
            return _db.Products.Select(p => new Product { id = p.id, status = p.status })
                 .Where(p => p.id != id && p.status == 0).Count();
        }

        public int CreateNewId() {
            int newId = 0;
            if (_db.Products.Any()) {
                newId = _db.Products.Max(x => x.id) + 1;
            }
            return newId;
        }

        public int CreateNewRatingId() {
            int newId = 0;
            if (_db.Ratings.Any()) {
                newId = _db.Ratings.Max(x => x.id) + 1;
            }
            return newId;
        }

        public async Task<Product> CreateProduct(Product product, int newId) {
            _db.Products.Add(product);

            await _db.SaveChangesAsync();

            Product product1 = await _db.Products.OrderBy(x => x.id)
                .LastOrDefaultAsync(i => i.id == newId);

            return product1;
        }

        public async Task<Rating> CreateRating(Rating rating, int newId) {

            _db.Ratings.Add(rating);

            await _db.SaveChangesAsync();

            Rating rating2 = await _db.Ratings.OrderBy(x => x.id)
                .LastOrDefaultAsync(i => i.id == newId);

            return rating2;
        }

        public async Task<bool> DeleteProduct(Product product) {
            bool isSuccess = false;
            Product product1 = _db.Products.Where(x => x.id == product.id).SingleOrDefault();
            if (product1 != null) {
                _db.Ratings.RemoveRange(_db.Ratings.Where(x => x.productId == product.id));
                _db.OrderDetails.RemoveRange(_db.OrderDetails.Where(x => x.productId == product.id));
                _db.Entry(product1).State = EntityState.Deleted;
                await _db.SaveChangesAsync();
                isSuccess = true;
            }
            return isSuccess;
        }

        public List<Product> GetAllProductsForAdmin(int skip, int limit) {
            return _db.Products.Select(x => new Product {
                id = x.id,
                name = x.name,
                imageUrl = x.imageUrl,
                status = x.status,
                color = x.color,
                price = x.price,
                discount = x.discount,
                description = x.description,
                quantity = x.quantity,
                type = x.type,
                categoryId = x.categoryId,
                category = new Category {
                    id = x.category.id,
                    title = x.category.title,
                    slug = x.category.slug
                },
                ratings = x.ratings.Select(r => new Rating {
                    id = r.id,
                    userId = r.userId,
                    productId = r.productId,
                    rating = r.rating,
                    description = r.description,
                    user = new Models.User {
                        id = r.user.id,
                        name = r.user.name,
                        updatedAt = r.user.updatedAt,
                    },
                    createdAt = r.createdAt,
                    updatedAt = r.updatedAt
                }).ToList(),
                createdAt = x.createdAt,
                updatedAt = x.updatedAt
            }).OrderBy(p => p.updatedAt).Skip(skip).Take((int)limit).ToList();
        }

        public List<Product> GetAllProductsForClient(int skip, int limit) {
            return _db.Products.Select(x => new Product {
                id = x.id,
                name = x.name,
                imageUrl = x.imageUrl,
                status = x.status,
                color = x.color,
                price = x.price,
                discount = x.discount,
                description = x.description,
                quantity = x.quantity,
                type = x.type,
                categoryId = x.categoryId,
                category = new Category {
                    id = x.category.id,
                    title = x.category.title,
                    slug = x.category.slug
                },
                ratings = x.ratings.Select(r => new Rating {
                    id = r.id,
                    userId = r.userId,
                    productId = r.productId,
                    rating = r.rating,
                    description = r.description,
                    ratingFor = r.ratingFor,
                    user = new User {
                        id = r.user.id,
                        name = r.user.name,
                        updatedAt = r.user.updatedAt,
                    },
                    createdAt = r.createdAt,
                    updatedAt = r.updatedAt
                }).ToList(),
                createdAt = x.createdAt,
                updatedAt = x.updatedAt
            }).Where(p => p.status == 0).OrderBy(p => p.updatedAt).Skip(0).Take((int)limit).ToList();
        }

        public Product GetById(int id) {
            return _db.Products
               .Include(i => i.category).Select(x => new Product {
                   id = x.id,
                   name = x.name,
                   imageUrl = x.imageUrl,
                   status = x.status,
                   color = x.color,
                   price = x.price,
                   discount = x.discount,
                   description = x.description,
                   quantity = x.quantity,
                   type = x.type,
                   categoryId = x.categoryId,
                   category = new Category {
                       id = x.category.id,
                       title = x.category.title,
                       slug = x.category.slug
                   },
                   ratings = x.ratings.Select(r => new Rating {
                       id = r.id,
                       userId = r.userId,
                       productId = r.productId,
                       rating = r.rating,
                       description = r.description,
                       user = new Models.User {
                           id = r.user.id,
                           name = r.user.name,
                           updatedAt = r.user.updatedAt,
                       },
                       createdAt = r.createdAt,
                       updatedAt = r.updatedAt
                   }).ToList(),
                   createdAt = x.createdAt,
                   updatedAt = x.updatedAt
               }).FirstOrDefault(x => x.id == id);
        }

        public List<Product> GetProductsByCategory(int skip, int limit, string slug) {
            return _db.Products.Select(x => new Product {
                id = x.id,
                name = x.name,
                imageUrl = x.imageUrl,
                status = x.status,
                color = x.color,
                price = x.price,
                discount = x.discount,
                description = x.description,
                quantity = x.quantity,
                type = x.type,
                categoryId = x.categoryId,
                category = new Category {
                    id = x.category.id,
                    title = x.category.title,
                    slug = x.category.slug
                },
                ratings = x.ratings.Select(r => new Rating {
                    id = r.id,
                    userId = r.userId,
                    productId = r.productId,
                    rating = r.rating,
                    description = r.description,
                    user = new Models.User {
                        id = r.user.id,
                        name = r.user.name,
                        updatedAt = r.user.updatedAt,
                    },
                    createdAt = r.createdAt,
                    updatedAt = r.updatedAt
                }).ToList(),
                createdAt = x.createdAt,
                updatedAt = x.updatedAt
            }).Where(p => p.category.slug == slug && p.status == 0).Skip(skip).Take((int)limit).ToList().OrderBy(p => p.updatedAt).ToList();
        }

        public List<Product> GetRelatedProducts(int id, int skip, int limit) {
            return _db.Products.Select(x => new Product {
                id = x.id,
                name = x.name,
                imageUrl = x.imageUrl,
                status = x.status,
                color = x.color,
                price = x.price,
                discount = x.discount,
                description = x.description,
                quantity = x.quantity,
                type = x.type,
                categoryId = x.categoryId,
                category = new Category {
                    id = x.category.id,
                    title = x.category.title,
                    slug = x.category.slug
                },
                ratings = x.ratings.Select(r => new Rating {
                    id = r.id,
                    userId = r.userId,
                    productId = r.productId,
                    rating = r.rating,
                    description = r.description,
                    user = new Models.User {
                        id = r.user.id,
                        name = r.user.name,
                        updatedAt = r.user.updatedAt,
                    },
                    createdAt = r.createdAt,
                    updatedAt = r.updatedAt
                }).ToList(),
                createdAt = x.createdAt,
                updatedAt = x.updatedAt
            }).Where(p => p.id != id && p.status == 0).OrderBy(p => p.updatedAt).Skip(skip).Take((int)limit).ToList();

        }

        public List<Product> HandleSort(ICollection<Product> products, string sortType) {
            switch (sortType) {
                case "priceLowHight": {
                        products = products.OrderBy(p => p.price).ToList();
                        break;
                    };
                case "priceHightLow": {

                        products = products.OrderByDescending(p => p.price).ToList();
                        break;
                    };
                case "nameAZ": {
                        products = products.OrderBy(p => p.name).ToList();
                        break;
                    }
                case "nameZA": {
                        products = products.OrderByDescending(p => p.name).ToList();
                        break;
                    }
                default: break;

            }
            return products.ToList();
        }

        public async Task<Product> UpdateProduct(Product product, int id) {
            var result = _db.Products.SingleOrDefault(b => b.id == id);
            if (result != null) {
                result.name = product.name;
                result.imageUrl = product.imageUrl;
                result.price = product.price;
                result.discount = product.discount;
                result.description = product.description;
                result.categoryId = product.categoryId;
                result.type = product.type;
                result.color = product.color;
                result.quantity = product.quantity;
                result.status = product.status;
                result.updatedAt = DateTime.UtcNow;
                await _db.SaveChangesAsync();
                Product product1 = await _db.Products.OrderBy(x => x.id)
                    .LastOrDefaultAsync(i => i.id == id);

                return product1;
            }
            return null;
        }
    }
}