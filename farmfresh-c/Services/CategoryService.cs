﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using farmfresh_c.Models;
using Microsoft.EntityFrameworkCore;

namespace farmfresh_c.Services {
    public interface ICategoryService {
        List<Category> GetCategoriesForClient(int skip, int limit);

        List<Category> GetCategoriesForAdmin(int skip, int limit);

        Category GetById(int id);

        Category GetByIdForAdmin(int id);

        int CountCategoryForClient();

        int CreateNewId();

        int CountCategory();

        Task<Category> CreateCategory(Category category, int newId);

        Task<bool> DeleteCategory(Category category);

        Task<Category> UpdateCategory(Category category, int id);
    }

    public class CategoryService : ICategoryService {
        public readonly DataContext _db;

        public CategoryService(DataContext db) {
            this._db = db;
        }

        public int CountCategory() {
            return _db.Categorys.Count();
        }

        public int CountCategoryForClient() {
            return _db.Categorys.Select(c => new Category {
                id = c.id,
                title = c.title,
                showToHomePage = c.showToHomePage,
                slug = c.slug,
                createdAt = c.createdAt,
                updatedAt = c.updatedAt
            }).Where(c => c.showToHomePage == true).Count();
        }

        public int CreateNewId() {
            int newId = 0;
            if (_db.Categorys.Any()) {
                newId = _db.Categorys.Max(x => x.id) + 1;
            }
            return newId;
        }

        public Category GetById(int id) {
            return _db.Categorys.Find(id);
        }

        public Category GetByIdForAdmin(int id) {
            return _db.Categorys.Find(id);
        }

        public List<Category> GetCategoriesForAdmin(int skip, int limit) {
            return _db.Categorys.Select(c => new Category {
                id = c.id,
                title = c.title,
                showToHomePage = c.showToHomePage,
                slug = c.slug,
                createdAt = c.createdAt,
                updatedAt = c.updatedAt
            }).Skip(skip).Take((int)limit).OrderBy(c => c.updatedAt).ToList();
        }

        public async Task<bool> DeleteCategory(Category category) {
            bool isSuccess = false;
            Category category1 = _db.Categorys.Where(x => x.id == category.id).SingleOrDefault();
            if (category1 != null) {
                _db.Products.RemoveRange(_db.Products.Where(x => x.categoryId == category.id));
                _db.OrderDetails.RemoveRange(_db.OrderDetails.Where(x => x.product.categoryId == category.id));
                _db.Entry(category1).State = EntityState.Deleted;
                await _db.SaveChangesAsync();
                isSuccess = true;
            }
            return isSuccess;
        }

        public List<Category> GetCategoriesForClient(int skip, int limit) {
            return _db.Categorys.Select(c => new Category {
                id = c.id,
                title = c.title,
                showToHomePage = c.showToHomePage,
                slug = c.slug,
                createdAt = c.createdAt,
                updatedAt = c.updatedAt
            }).Where(c => c.showToHomePage == true).Skip(skip).Take((int)limit).OrderBy(c => c.updatedAt).ToList();
        }

        public async Task<Category> CreateCategory(Category category, int newId) {
            _db.Categorys.Add(category);

            await _db.SaveChangesAsync();

            Category category1 = await _db.Categorys.OrderBy(x => x.id)
                .LastOrDefaultAsync(i => i.id == newId);

            return category1;
        }

        public async Task<Category> UpdateCategory(Category category, int id) {
            var result = _db.Categorys.SingleOrDefault(b => b.id == id);
            if (result != null) {
                result.title = category.title;
                result.slug = category.slug;
                result.showToHomePage = category.showToHomePage;
                result.updatedAt = DateTime.UtcNow;
                await _db.SaveChangesAsync();
                Category category1 = await _db.Categorys.OrderBy(x => x.id)
                    .LastOrDefaultAsync(i => i.id == id);

                return category1;
            }
            return null;
        }
    }
}