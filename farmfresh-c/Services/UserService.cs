using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using farmfresh_c.Models;
using BC = BCrypt.Net.BCrypt;
using System.Threading.Tasks;
using farmfresh_c.DTO;
using farmfresh_c.DTO.user;

namespace farmfresh_c.Services {
    public interface IUserService {
        List<User> GetAllUsers(int roleId, int skip, int limit);

        User GetById(int id);

        User GetByIdForAdmin(int id);

        User FindUserByEmail(string email);

        User FindUserByEmailForLogin(string email);

        bool VerifyPassword(User u, string password);

        string generateJwtToken(User user, int day);

        int CountByRole(int roleId);

        User GetUserByAccessToken(string accessToken);

        int CreateNewUserId();

        Task<User> SaveUser(User user, int roleId);

        void SaveLogin(int id, string accessToken, string refreshToken);

        User GetUserByBasicAuthor(string token);

        void UpdateInfo(UserCreateDTO userCreateDTO);

        void UpdateInfoForAdmin(UserUpdateForAdminDTO userCreateDTO);
    }

    public class UserService : IUserService {
        public readonly DataContext _db;

        public UserService(DataContext db) {
            this._db = db;
        }

        public User FindUserByEmail(string email) {
            return _db.Users.Select(u => new User { id = u.id, email = u.email }).Where(u => u.email == email).FirstOrDefault();
        }

        public List<User> GetAllUsers(int roleId, int skip, int limit) {
            return _db.Users.Select(x => new User {
                name = x.name,
                email = x.email,
                firstName = x.firstName,
                lastName = x.lastName,
                phone = x.phone,
                status = x.status,
                province = x.province,
                district = x.district,
                address = x.address,
                cartSaved = x.cartSaved,
                lastLogin = x.lastLogin,
                role = new Role { id = x.role.id, name = x.role.name },
                roleId = x.roleId,
                productLikes = null,
                rejectReason = x.rejectReason,
                id = x.id,
                createdAt = x.createdAt,
                updatedAt = x.updatedAt,
            }).Where(u => u.roleId == roleId).Skip(skip).Take(limit).OrderBy(u => u.updatedAt).ToList();
        }

        public User GetById(int id) {
            return _db.Users.Select(x => new User {
                name = x.name,
                email = x.email,
                firstName = x.firstName,
                lastName = x.lastName,
                phone = x.phone,
                status = x.status,
                province = x.province,
                district = x.district,
                address = x.address,
                cartSaved = x.cartSaved,
                accessToken = x.accessToken,
                refreshToken = x.refreshToken,
                lastLogin = x.lastLogin,
                role = new Role { id = x.role.id, name = x.role.name },
                roleId = x.roleId,
                ratings = x.ratings.Select(r => new Rating {
                    id = r.id,
                    userId = r.userId,
                    productId = r.productId,
                    blogId = r.blogId,
                    ratingFor = r.ratingFor,
                    description = r.description,
                    rating = r.rating,
                    createdAt = r.createdAt,
                    updatedAt = r.updatedAt
                }).ToList(),
                blogs = x.blogs.Select(b => new Blog {
                    id = b.id,
                    title = b.title,
                    content = b.content,
                    createdAt = b.createdAt,
                    updatedAt = b.updatedAt,
                    userId = b.userId,
                    image = b.image,
                    slug = b.slug,
                    status = b.status
                }).ToList(),
                orders = x.orders.Select(o => new Order {
                    id = o.id,
                    userId = o.userId,
                    province = o.province,
                    district = o.district,
                    address = o.address,
                    phone = o.phone,
                    firstName = o.firstName,
                    lastName = o.lastName,
                    totalMoney = o.totalMoney,
                    deliveryDate = o.deliveryDate,
                    orderDetails = o.orderDetails.Select(od => new OrderDetail { id = od.id, product = od.product, orderId = od.orderId }).ToList()
                }).ToList(),
                productLikes = null,
                id = x.id,
                createdAt = x.createdAt,
                updatedAt = x.updatedAt,
            }).FirstOrDefault(x => x.id == id);
        }

        public User GetByIdForAdmin(int id) {
            return _db.Users.Select(x => new User {
                name = x.name,
                email = x.email,
                firstName = x.firstName,
                lastName = x.lastName,
                phone = x.phone,
                status = x.status,
                province = x.province,
                district = x.district,
                address = x.address,
                cartSaved = x.cartSaved,
                accessToken = x.accessToken,
                refreshToken = x.refreshToken,
                lastLogin = x.lastLogin,
                password = x.password,
                role = new Role { id = x.role.id, name = x.role.name },
                roleId = x.roleId,
                ratings = x.ratings.Select(r => new Rating {
                    id = r.id,
                    userId = r.userId,
                    productId = r.productId,
                    blogId = r.blogId,
                    ratingFor = r.ratingFor,
                    description = r.description,
                    rating = r.rating,
                    createdAt = r.createdAt,
                    updatedAt = r.updatedAt
                }).ToList(),
                blogs = x.blogs.Select(b => new Blog {
                    id = b.id,
                    title = b.title,
                    content = b.content,
                    createdAt = b.createdAt,
                    updatedAt = b.updatedAt,
                    userId = b.userId,
                    image = b.image,
                    slug = b.slug,
                    status = b.status
                }).ToList(),
                orders = x.orders.Select(o => new Order {
                    id = o.id,
                    userId = o.userId,
                    province = o.province,
                    district = o.district,
                    address = o.address,
                    phone = o.phone,
                    firstName = o.firstName,
                    lastName = o.lastName,
                    totalMoney = o.totalMoney,
                    deliveryDate = o.deliveryDate,
                    orderDetails = o.orderDetails.Select(od => new OrderDetail { id = od.id, product = od.product, orderId = od.orderId }).ToList()
                }).ToList(),
                productLikes = null,
                id = x.id,
                createdAt = x.createdAt,
                updatedAt = x.updatedAt,
            }).FirstOrDefault(x => x.id == id);
        }

        public bool VerifyPassword(User user, string password) {
            return BC.Verify(password, user.password);
        }


        // helper methods

        public string NullCheck(string value) {
            return value ?? string.Empty;
        }

        public string generateJwtToken(User user, int day) {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("RANDOM_TOKEN_SECRET");
            var tokenDescriptor = new SecurityTokenDescriptor {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.id.ToString()),
                 new Claim("name", user.name),
                    new Claim("email", user.email),
                    new Claim("roleId", user.roleId.ToString()),
                    new Claim("firstName",NullCheck(user.firstName)),
                    new Claim("lastName", NullCheck(user.lastName)),
                    new Claim("phone", NullCheck(user.phone)),
                    new Claim("province", NullCheck(user.province)),
                    new Claim("district", NullCheck(user.district)),
                    new Claim("address", NullCheck(user.address)),
                }),
                Expires = DateTime.UtcNow.AddDays(day),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public int CountByRole(int roleId) {
            return _db.Users.Select(u => new User { id = u.id, roleId = u.roleId }).Count();
        }

        public User GetUserByAccessToken(string accessToken) {
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(accessToken);
            var tokenS = jsonToken as JwtSecurityToken;
            var idString = tokenS.Claims.First(claim => claim.Type == "id").Value;
            int id = int.Parse(idString);
            return _db.Users.Select(x => new User {
                name = x.name,
                email = x.email,
                firstName = x.firstName,
                lastName = x.lastName,
                phone = x.phone,
                status = x.status,
                province = x.province,
                district = x.district,
                address = x.address,
                cartSaved = x.cartSaved,
                accessToken = x.accessToken,
                refreshToken = x.refreshToken,
                lastLogin = x.lastLogin,
                role = new Role { id = x.role.id, name = x.role.name },
                roleId = x.roleId,
                ratings = x.ratings.Select(r => new Rating {
                    id = r.id,
                    userId = r.userId,
                    productId = r.productId,
                    blogId = r.blogId,
                    ratingFor = r.ratingFor,
                    description = r.description,
                    rating = r.rating,
                    createdAt = r.createdAt,
                    updatedAt = r.updatedAt
                }).ToList(),
                blogs = x.blogs.Select(b => new Blog {
                    id = b.id,
                    title = b.title,
                    content = b.content,
                    createdAt = b.createdAt,
                    updatedAt = b.updatedAt,
                    userId = b.userId,
                    image = b.image,
                    slug = b.slug,
                    status = b.status
                }).ToList(),
                orders = x.orders.Select(o => new Order {
                    id = o.id,
                    userId = o.userId,
                    province = o.province,
                    district = o.district,
                    address = o.address,
                    phone = o.phone,
                    firstName = o.firstName,
                    lastName = o.lastName,
                    totalMoney = o.totalMoney,
                    deliveryDate = o.deliveryDate,
                    orderDetails = o.orderDetails.Select(od => new OrderDetail { id = od.id, product = od.product, orderId = od.orderId }).ToList()
                }).ToList(),
                productLikes = null,
                id = x.id,
                createdAt = x.createdAt,
                updatedAt = x.updatedAt,
            }).FirstOrDefault(x => x.id == id);
        }

        public int CreateNewUserId() {
            int newId = 0;
            if (_db.Users.Any()) {
                newId = _db.Users.Max(x => x.id) + 1;
            }
            return newId;
        }

        public async Task<User> SaveUser(User user, int roleId) {
            string newPass = BC.HashPassword(user.password, 10);
            user.roleId = roleId;
            user.password = newPass;
            user.createdAt = DateTime.UtcNow;
            user.updatedAt = DateTime.UtcNow;
            _db.Users.Add(user);
            await _db.SaveChangesAsync();
            return user;
        }

        public void SaveLogin(int id, string accessToken, string refreshToken) {
            var user = _db.Users.FirstOrDefault(u => u.id == id);
            user.accessToken = accessToken;
            user.refreshToken = refreshToken;
            user.lastLogin = DateTime.Now;
            _db.SaveChanges();
        }

        public User FindUserByEmailForLogin(string email) {
            return _db.Users.Select(u => new User {
                id = u.id,
                email = u.email,
                password = u.password,
                name = u.name,
                roleId = u.roleId,
                firstName = u.firstName,
                lastName = u.lastName,
                province = u.province,
                phone = u.phone,
                district = u.district,
                address = u.address,
                status = u.status,
                rejectReason = u.rejectReason
            })
                .Where(u => u.email == email).FirstOrDefault();
        }

        public User GetUserByBasicAuthor(string token) {
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(token));
            int seperatorIndex = usernamePassword.IndexOf(':');

            string email = usernamePassword.Substring(0, seperatorIndex);
            string password = usernamePassword.Substring(seperatorIndex + 1);

            User user = _db.Users.Select(u => new User { id = u.id, email = u.email, password = u.password }).Where(u => u.email == email).FirstOrDefault();
            if (user != null && VerifyPassword(user, password)) {
                User u1 = GetById(user.id);
                return u1;
            }
            return null;
        }

        public void UpdateInfo(UserCreateDTO userCreateDTO) {
            var user = _db.Users.FirstOrDefault(u => u.email == userCreateDTO.email);
            user.address = userCreateDTO.address;
            user.name = userCreateDTO.name;
            user.firstName = userCreateDTO.firstName;
            user.lastName = userCreateDTO.lastName;
            user.phone = userCreateDTO.phone;
            user.province = userCreateDTO.province;
            user.status = 0;
            user.password = BC.HashPassword(userCreateDTO.password, 10);
            user.district = userCreateDTO.district;
            user.updatedAt = DateTime.UtcNow;
            _db.SaveChanges();
        }

        public void UpdateInfoForAdmin(UserUpdateForAdminDTO userCreateDTO) {
            var user = _db.Users.FirstOrDefault(u => u.email == userCreateDTO.email);
            user.address = userCreateDTO.address;
            user.name = userCreateDTO.name;
            user.firstName = userCreateDTO.firstName;
            user.lastName = userCreateDTO.lastName;
            user.phone = userCreateDTO.phone;
            user.province = userCreateDTO.province;
            user.password = userCreateDTO.password;
            user.status = (int)userCreateDTO.status;
            user.rejectReason = userCreateDTO.rejectReason;
            user.district = userCreateDTO.district;
            user.updatedAt = DateTime.UtcNow;
            user.roleId = userCreateDTO.roleId;
            _db.SaveChanges();
        }
    }
}