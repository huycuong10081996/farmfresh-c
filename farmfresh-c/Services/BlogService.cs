﻿using System.Collections.Generic;
using System.Linq;
using farmfresh_c.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace farmfresh_c.Services {
    public interface IBlogService {
        List<Blog> GetAllBlogs(int skip, int limit);

        Blog GetBySlug(string slug);

        int CountBlog();

        Task<Blog> CreateBlog(Blog blog);

        int CreateNewId();
    }

    public class BlogService : IBlogService {
        public readonly DataContext _db;

        public BlogService(DataContext db) {
            this._db = db;
        }

        public int CountBlog() {
            return _db.Blogs.Count();
        }

        public async Task<Blog> CreateBlog(Blog blog) {
            _db.Blogs.Add(blog);

            await _db.SaveChangesAsync();

            var blog1 = await _db.Blogs
                .Include(i => i.user).OrderBy(x => x.id)
                .LastOrDefaultAsync(i => i.userId == blog.userId);
            return blog1;
        }

        public int CreateNewId() {
            int newId = 0;
            if (_db.Blogs.Any()) {
                newId = _db.Blogs.Max(x => x.id) + 1;
            }
            return newId;
        }

        public List<Blog> GetAllBlogs(int skip, int limit) {
            return _db.Blogs.Select(x =>
                  new Blog {
                      id = x.id,
                      title = x.title,
                      content = x.content,
                      image = x.image,
                      status = x.status,
                      slug = x.slug,
                      userId = x.userId,
                      user = new User { id = x.user.id, name = x.user.name },
                      createdAt = x.createdAt,
                      updatedAt = x.updatedAt
                  }
              ).Skip(skip).Take((int)limit).OrderBy(x => x.updatedAt).ToList();
        }

        public Blog GetBySlug(string slug) {
            return _db.Blogs
              .Include(i => i.user).Select(x => new Blog {
                  id = x.id,
                  title = x.title,
                  content = x.content,
                  image = x.image,
                  status = x.status,
                  slug = x.slug,
                  userId = x.userId,
                  user = new User { id = x.user.id, name = x.user.name },
                  createdAt = x.createdAt,
                  updatedAt = x.updatedAt
              }).FirstOrDefault(x => x.slug == slug);
        }
    }
}