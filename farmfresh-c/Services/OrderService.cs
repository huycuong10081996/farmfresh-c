﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using farmfresh_c.DTO;
using farmfresh_c.Models;
using Microsoft.EntityFrameworkCore;

namespace farmfresh_c.Services {
    public interface IOrderService {
        int CreateNewId();

        int CountOrder();

        Order GetById(int id);

        int CountOrderByUserId(int userId);

        List<Order> GetOrderByUserId(int userId, int skip, int limit);

        List<Order> GetOrders(int skip, int limit);

        Task<OrderDetail> CreateOrderDetail(OrderDetail orderDetail);

        Task<Order> CreateOrder(Order order);

        Task<Order> UpdateOrder(Order order, int id);

        Collection<OrderDetail> ToCollection(List<OrderDetailCreateDto> items, int id);
    }

    public class OrderService : IOrderService {
        public readonly DataContext _db;

        public OrderService(DataContext db) {
            this._db = db;
        }

        public int CountOrder() {
            return _db.Orders.Count();
        }

        public int CountOrderByUserId(int userId) {
            return _db.Orders.Select(o => new Order { userId = o.userId }).Where(o => o.userId == userId).Count();
        }

        public int CreateNewId() {
            int newId = 0;
            if (_db.Orders.Any()) {
                newId = _db.Orders.Max(x => x.id) + 1;
            }
            return newId;
        }

        public async Task<Order> CreateOrder(Order order) {
            _db.Orders.Add(order);

            await _db.SaveChangesAsync();
            return order;
        }

        public async Task<OrderDetail> CreateOrderDetail(OrderDetail orderDetail) {
            _db.OrderDetails.Add(orderDetail);

            await _db.SaveChangesAsync();
            return orderDetail;
        }

        public Collection<OrderDetail> ToCollection(List<OrderDetailCreateDto> items, int id) {
            Collection<OrderDetail> collection = new Collection<OrderDetail>();

            for (int i = 0; i < items.Count; i++) {
                OrderDetail orderDetail = new OrderDetail((int)items[i].quantity, items[i].product.id, id);
                collection.Add(orderDetail);
            }

            return collection;
        }

        public List<Order> GetOrders(int skip, int limit) {
            return _db.Orders.Select(c => new Order {
                id = c.id,
                district = c.district,
                address = c.address,
                province = c.province,
                firstName = c.firstName,
                lastName = c.lastName,
                phone = c.phone,
                deliveryDate = c.deliveryDate,
                status = c.status,
                totalMoney = c.totalMoney,
                orderDetails = c.orderDetails.Select(x => new OrderDetail {
                    id = x.id,
                    quantity = x.quantity,
                    productId = x.productId,
                    orderId = x.orderId,
                    product = new Product {
                        id = x.product.id,
                        name = x.product.name,
                        imageUrl = x.product.imageUrl,
                        status = x.product.status,
                        color = x.product.color,
                        price = x.product.price,
                        discount = x.product.discount,
                        description = x.product.description,
                        quantity = x.product.quantity,
                        type = x.product.type,
                        categoryId = x.product.categoryId,
                        category = new Category {
                            id = x.product.category.id,
                            title = x.product.category.title,
                            slug = x.product.category.slug
                        },
                        ratings = x.product.ratings.Select(r => new Rating {
                            id = r.id,
                            userId = r.userId,
                            productId = r.productId,
                            rating = r.rating,
                            description = r.description,
                            user = new Models.User {
                                id = r.user.id,
                                name = r.user.name,
                                updatedAt = r.user.updatedAt,
                            },
                            createdAt = r.createdAt,
                            updatedAt = r.updatedAt
                        }).ToList(),
                        createdAt = x.product.createdAt,
                        updatedAt = x.product.updatedAt
                    },
                }).ToList(),
                user = new User {
                    id = c.user.id,
                    email = c.user.email,
                    name = c.user.name,
                    firstName = c.user.firstName,
                    lastName = c.user.lastName
                },
                createdAt = c.createdAt,
                updatedAt = c.updatedAt,
                userId = c.userId,
            }).Skip(skip).Take((int)limit).OrderBy(c => c.updatedAt).ToList();

        }

        public List<Order> GetOrderByUserId(int userId, int skip, int limit) {
            return _db.Orders.Select(c => new Order {
                id = c.id,
                district = c.district,
                address = c.address,
                province = c.province,
                firstName = c.firstName,
                lastName = c.lastName,
                phone = c.phone,
                deliveryDate = c.deliveryDate,
                status = c.status,
                totalMoney = c.totalMoney,
                orderDetails = c.orderDetails.Select(x => new OrderDetail {
                    id = x.id,
                    quantity = x.quantity,
                    productId = x.productId,
                    orderId = x.orderId,
                    product = new Product {
                        id = x.product.id,
                        name = x.product.name,
                        imageUrl = x.product.imageUrl,
                        status = x.product.status,
                        color = x.product.color,
                        price = x.product.price,
                        discount = x.product.discount,
                        description = x.product.description,
                        quantity = x.product.quantity,
                        type = x.product.type,
                        categoryId = x.product.categoryId,
                        category = new Category {
                            id = x.product.category.id,
                            title = x.product.category.title,
                            slug = x.product.category.slug
                        },
                        ratings = x.product.ratings.Select(r => new Rating {
                            id = r.id,
                            userId = r.userId,
                            productId = r.productId,
                            rating = r.rating,
                            description = r.description,
                            user = new Models.User {
                                id = r.user.id,
                                name = r.user.name,
                                updatedAt = r.user.updatedAt,
                            },
                            createdAt = r.createdAt,
                            updatedAt = r.updatedAt
                        }).ToList(),
                        createdAt = x.product.createdAt,
                        updatedAt = x.product.updatedAt
                    },
                }).ToList(),
                createdAt = c.createdAt,
                updatedAt = c.updatedAt,
                userId = c.userId,
            }).Where(o => o.userId == userId).Skip(skip).Take((int)limit).OrderBy(c => c.updatedAt).ToList();

        }

        public async Task<Order> UpdateOrder(Order order, int id) {
            var result = _db.Orders.SingleOrDefault(b => b.id == id);
            if (result != null) {
                result.firstName = order.firstName;
                result.lastName = order.lastName;
                result.district = order.district;
                result.phone = order.phone;
                result.province = order.province;
                result.deliveryDate = order.deliveryDate;
                result.status = order.status;
                result.address = order.address;
                result.updatedAt = DateTime.UtcNow;
                await _db.SaveChangesAsync();

                Order order1 = await _db.Orders.OrderBy(x => x.id)
                    .LastOrDefaultAsync(i => i.id == id);

                return order1;
            }
            return null;
        }

        public Order GetById(int id) {
            return _db.Orders.Select(c => new Order {
                id = c.id,
                district = c.district,
                address = c.address,
                province = c.province,
                firstName = c.firstName,
                lastName = c.lastName,
                phone = c.phone,
                deliveryDate = c.deliveryDate,
                status = c.status,
                totalMoney = c.totalMoney,
                orderDetails = c.orderDetails.Select(x => new OrderDetail {
                    id = x.id,
                    quantity = x.quantity,
                    productId = x.productId,
                    orderId = x.orderId,
                    product = new Product {
                        id = x.product.id,
                        name = x.product.name,
                        imageUrl = x.product.imageUrl,
                        status = x.product.status,
                        color = x.product.color,
                        price = x.product.price,
                        discount = x.product.discount,
                        description = x.product.description,
                        quantity = x.product.quantity,
                        type = x.product.type,
                        categoryId = x.product.categoryId,
                        category = new Category {
                            id = x.product.category.id,
                            title = x.product.category.title,
                            slug = x.product.category.slug
                        },
                        ratings = x.product.ratings.Select(r => new Rating {
                            id = r.id,
                            userId = r.userId,
                            productId = r.productId,
                            rating = r.rating,
                            description = r.description,
                            user = new Models.User {
                                id = r.user.id,
                                name = r.user.name,
                                updatedAt = r.user.updatedAt,
                            },
                            createdAt = r.createdAt,
                            updatedAt = r.updatedAt
                        }).ToList(),
                        createdAt = x.product.createdAt,
                        updatedAt = x.product.updatedAt
                    },
                }).ToList(),
                createdAt = c.createdAt,
                updatedAt = c.updatedAt,
                userId = c.userId,
                user = new User { id = c.user.id, name = c.user.name, email = c.user.email }
            }).Where(o => o.id == id).FirstOrDefault();
        }
    }
}
