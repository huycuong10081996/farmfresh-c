﻿-- -------------------------------------------------------------
-- TablePlus 3.12.4(360)
--
-- https://tableplus.com/
--
-- Database: FARMFRESH-NODE
-- Generation Time: 2021-05-17 12:18:58.8650
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `Blog`;
CREATE TABLE `Blog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `slug` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `Category`;
CREATE TABLE `Category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `showToHomePage` tinyint(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `Order`;
CREATE TABLE `Order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `deliveryDate` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `totalMoney` double DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `OrderDetail`;
CREATE TABLE `OrderDetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quantity` int DEFAULT NULL,
  `orderId` int DEFAULT NULL,
  `productId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderId` (`orderId`),
  KEY `productId` (`productId`),
  CONSTRAINT `orderdetail_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `Order` (`id`),
  CONSTRAINT `orderdetail_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `Product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `Product`;
CREATE TABLE `Product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `imageUrl` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `description` text,
  `quantity` int DEFAULT NULL,
  `categoryId` int DEFAULT NULL,
  `showToHomePage` json DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `Category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `Product_Like`;
CREATE TABLE `Product_Like` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `productId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `productId` (`productId`),
  CONSTRAINT `product_like_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `User` (`id`),
  CONSTRAINT `product_like_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `Product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `Rating`;
CREATE TABLE `Rating` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `productId` int DEFAULT NULL,
  `blogId` int DEFAULT NULL,
  `ratingFor` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `productId` (`productId`),
  KEY `blogId` (`blogId`),
  CONSTRAINT `rating_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `User` (`id`),
  CONSTRAINT `rating_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `Product` (`id`),
  CONSTRAINT `rating_ibfk_3` FOREIGN KEY (`blogId`) REFERENCES `Blog` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `accessToken` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `refreshToken` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `lastLogin` datetime DEFAULT NULL,
  `cartSaved` json DEFAULT NULL,
  `roleId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `roleId` (`roleId`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `Role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Blog` (`id`, `userId`, `title`, `content`, `image`, `status`, `slug`, `createdAt`, `updatedAt`) VALUES
('33', '1', 'Necessitatibus Saepe', '<p><strong>The standard Lorem Ipsum passage, used since the 1500s</strong></p><p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>', 'https://res.cloudinary.com/farmfresh-c/image/upload/v1621227614/blog-5-890x620_rx4luf.jpg', '1', '123', '2021-04-27 18:45:46', '2021-04-27 18:45:46'),
('34', '1', 'Urna Curabitur', '<p><strong>The standard Lorem Ipsum passage, used since the 1500s</strong></p><p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>', 'https://res.cloudinary.com/farmfresh-c/image/upload/v1621227614/blog-6-890x620_zzslzp.jpg', '1', '456', '2021-04-27 18:45:48', '2021-04-27 18:45:48'),
('35', '1', 'Nullam ullamcorper', '<p><strong>The standard Lorem Ipsum passage, used since the 1500s</strong></p><p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>', 'https://res.cloudinary.com/farmfresh-c/image/upload/v1621227614/blog-4-890x620_vo3syk.jpg', '1', '789', '2021-04-27 18:45:49', '2021-04-27 18:45:49');

INSERT INTO `Category` (`id`, `title`, `slug`, `showToHomePage`, `createdAt`, `updatedAt`) VALUES
('1', 'Organic Food', 'organic-food', '1', '2020-10-25 03:54:46', '2020-10-25 03:54:46'),
('2', 'Chemical Free', 'chemical-free', '1', '2020-10-25 03:54:46', '2020-10-25 03:54:46'),
('3', 'Fruits', 'fruits', '1', '2020-10-25 03:54:46', '2020-10-25 03:54:46'),
('4', 'Nursery', 'nursery', '1', '2020-10-25 03:54:46', '2020-10-25 03:54:46'),
('5', 'Vegetables', 'vegetables', '1', '2020-10-25 03:54:46', '2020-10-25 03:54:46');

INSERT INTO `Product` (`id`, `name`, `imageUrl`, `status`, `price`, `discount`, `type`, `color`, `description`, `quantity`, `categoryId`, `showToHomePage`, `createdAt`, `updatedAt`) VALUES
('20', 'Apple', 'https://res.cloudinary.com/farmfresh-c/image/upload/v1621226888/cqnsqn7bc3riika2ikdj.jpg', '0', '104', NULL, 'weight', '#d0021b', '<p>Stop your co-workers in their tracks with the stunning new 30-inch diagonal HP LP3065 Flat Panel Monitor. This flagship monitor features best-in-class performance and presentation features on a huge wide-aspect screen while letting you work as comfortably as possible - you might even forget you\'re at the office</p>', '1000', '1', NULL, '2021-05-17 04:49:16', '2021-05-17 04:49:16'),
('21', 'Black Grape', 'https://res.cloudinary.com/farmfresh-c/image/upload/v1621227070/nvf7tmzxqaytxcipirxo.jpg', '0', '140', NULL, 'weight', '#000000', '<p><strong>More room to move.</strong></p><p>With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.</p><p><strong>Cover Flow.</strong></p><p>Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.</p><p><strong>Enhanced interface.</strong></p><p>Experience a whole new way to browse and view your music and video.</p><p><strong>Sleeker design.</strong></p><p>Beautiful, durable, and sleeker than ever, iPod classic now features an anodized aluminum and polished stainless steel enclosure with rounded edges.</p>', '1000', '3', NULL, '2021-05-17 04:51:41', '2021-05-17 04:51:41'),
('22', 'Cherry', 'https://res.cloudinary.com/farmfresh-c/image/upload/v1621228320/htfgqefayl6txjivgsju.jpg', '0', '118', '0.15', 'weight', '#d0021b', '<p><strong>Latest Intel mobile architecture</strong></p><p>Powered by the most advanced mobile processors from Intel, the new Core 2 Duo MacBook Pro is over 50% faster than the original Core Duo MacBook Pro and now supports up to 4GB of RAM.</p><p><strong>Leading-edge graphics</strong></p><p>The NVIDIA GeForce 8600M GT delivers exceptional graphics processing power. For the ultimate creative canvas, you can even configure the 17-inch model with a 1920-by-1200 resolution display.</p><p><strong>Designed for life on the road</strong></p><p>Innovations such as a magnetic power connection and an illuminated keyboard with ambient light sensor put the MacBook Pro in a class by itself.</p><p><strong>Connect. Create. Communicate.</strong></p><p>Quickly set up a video conference with the built-in iSight camera. Control presentations and media from up to 30 feet away with the included Apple Remote. Connect to high-bandwidth peripherals with FireWire 800 and DVI.</p><p><strong>Next-generation wireless</strong></p><p>Featuring 802.11n wireless technology, the MacBook Pro delivers up to five times the performance and up to twice the range of previous-generation technologies.</p>', '1000', '4', NULL, '2021-05-17 05:12:45', '2021-05-17 05:12:45');

INSERT INTO `Role` (`id`, `name`, `status`) VALUES
('1', 'Admin', '0'),
('2', 'Moderator', '0'),
('3', 'User', '0');

INSERT INTO `User` (`id`, `name`, `email`, `password`, `firstName`, `lastName`, `phone`, `status`, `province`, `district`, `address`, `accessToken`, `refreshToken`, `lastLogin`, `cartSaved`, `roleId`, `createdAt`, `updatedAt`) VALUES
('1', 'admin', 'admin@gmail.com', '$2b$10$mLowsmY5POHds1qQzU1CN.F98JrgB319BmGhUZTIBvl7Kx/oOrsVq', 'faw', 'fwa', '090890890', '0', NULL, NULL, NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJuYW1lIjoiYWRtaW4iLCJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsInJvbGVJZCI6IjEiLCJmaXJzdE5hbWUiOiJmYXciLCJsYXN0TmFtZSI6ImZ3YSIsInBob25lIjoiMDkwODkwODkwIiwicHJvdmluY2UiOiIiLCJkaXN0cmljdCI6IiIsImFkZHJlc3MiOiIiLCJuYmYiOjE2MjEyMjI0MzEsImV4cCI6MTYyMTgyNzIzMSwiaWF0IjoxNjIxMjIyNDMxfQ.Ozi4zjl9pzzbpwzVZ6Lv8E3sjqU1Hqi7x_hslvABuEE', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJuYW1lIjoiYWRtaW4iLCJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsInJvbGVJZCI6IjEiLCJmaXJzdE5hbWUiOiJmYXciLCJsYXN0TmFtZSI6ImZ3YSIsInBob25lIjoiMDkwODkwODkwIiwicHJvdmluY2UiOiIiLCJkaXN0cmljdCI6IiIsImFkZHJlc3MiOiIiLCJuYmYiOjE2MjEyMjI0MzEsImV4cCI6MTY1Mjc1ODQzMSwiaWF0IjoxNjIxMjIyNDMxfQ.JgXZ0QCnXhC8ASxSeZ7wOSrxSOD7KpXYvq4N7fxjmtA', '2021-05-17 10:33:52', NULL, '1', '2020-10-25 03:54:46', '2021-05-06 05:17:59');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;