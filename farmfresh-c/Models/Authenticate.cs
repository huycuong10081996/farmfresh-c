﻿using System;
namespace farmfresh_c.Models {
    public class Authenticate {
        public string refreshToken;
        public string accessToken;
        public Authenticate() {
        }

        public Authenticate(string refreshToken, string accessToken) {
            this.refreshToken = refreshToken;
            this.accessToken = accessToken;
        }
    }
}
