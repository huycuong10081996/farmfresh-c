﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace farmfresh_c.Models {
    [Table("User")]
    public class User : BaseModel {

        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phone { get; set; }
        public int status { get; set; }
        public string province { get; set; }
        public string district { get; set; }
        public string address { get; set; }
        public string cartSaved { get; set; }
        public string accessToken { get; set; }
        public string refreshToken { get; set; }
        public string rejectReason { get; set; }
        public DateTime lastLogin { get; set; }
        public Role role { get; set; }
        public int roleId { get; set; }
        public virtual ICollection<Rating> ratings { get; set; }
        public virtual ICollection<Blog> blogs { get; set; }
        public virtual ICollection<Order> orders { get; set; }
        public virtual ICollection<Product_Like> productLikes { get; set; }
        public User() {
        }


    }
}
