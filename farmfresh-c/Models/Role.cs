﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace farmfresh_c.Models {
    [Table("Role")]
    public class Role : BaseModel {

        public string name { get; set; }
        public int status { get; set; }
        public virtual ICollection<User> users { get; set; }
        public Role() {
        }
    }
}
