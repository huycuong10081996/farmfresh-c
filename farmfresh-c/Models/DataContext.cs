﻿using Microsoft.EntityFrameworkCore;

namespace farmfresh_c.Models {
    public class DataContext : DbContext {
        public DataContext(DbContextOptions<DataContext> options)
              : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            //config primary key(product,category)
            modelBuilder.Entity<Product>().Property(f => f.id).HasAnnotation("MySql:ValueGeneratedOnAdd", true).ValueGeneratedOnAdd();
            modelBuilder.Entity<Category>().Property(f => f.id).HasAnnotation("MySql:ValueGeneratedOnAdd", true).ValueGeneratedOnAdd();
            modelBuilder.Entity<Blog>().Property(f => f.id).HasAnnotation("MySql:ValueGeneratedOnAdd", true).ValueGeneratedOnAdd();
            modelBuilder.Entity<User>().Property(f => f.id).HasAnnotation("MySql:ValueGeneratedOnAdd", true).ValueGeneratedOnAdd();
            modelBuilder.Entity<Order>().Property(f => f.id).HasAnnotation("MySql:ValueGeneratedOnAdd", true).ValueGeneratedOnAdd();
            modelBuilder.Entity<OrderDetail>().Property(f => f.id).HasAnnotation("MySql:ValueGeneratedOnAdd", true).ValueGeneratedOnAdd();
            modelBuilder.Entity<Rating>().Property(f => f.id).HasAnnotation("MySql:ValueGeneratedOnAdd", true).ValueGeneratedOnAdd();
            modelBuilder.Entity<Product_Like>().Property(f => f.id).HasAnnotation("MySql:ValueGeneratedOnAdd", true).ValueGeneratedOnAdd();
            modelBuilder.Entity<Role>().Property(f => f.id).HasAnnotation("MySql:ValueGeneratedOnAdd", true).ValueGeneratedOnAdd();


            modelBuilder.Entity<Product>().HasKey(s => s.id);
            modelBuilder.Entity<Category>().HasKey(s => s.id);
            modelBuilder.Entity<Blog>().HasKey(s => s.id);
            modelBuilder.Entity<User>().HasKey(s => s.id);
            modelBuilder.Entity<Order>().HasKey(s => s.id);
            modelBuilder.Entity<OrderDetail>().HasKey(s => s.id);
            modelBuilder.Entity<Rating>().HasKey(s => s.id);
            modelBuilder.Entity<Product_Like>().HasKey(s => s.id);
            modelBuilder.Entity<Role>().HasKey(s => s.id);

            //set config replationship
            //Blog
            modelBuilder.Entity<Blog>()
                .HasMany<Rating>(s => s.ratings)
                .WithOne(a => a.blog)
                .HasForeignKey(a => a.blogId)
                .OnDelete(DeleteBehavior.Restrict);

            // Category
            modelBuilder.Entity<Category>()
                .HasMany<Product>(s => s.products)
                .WithOne(a => a.category)
                .HasForeignKey(a => a.categoryId)
                .OnDelete(DeleteBehavior.Restrict);

            // User
            modelBuilder.Entity<User>().
                HasMany<Rating>(s => s.ratings).
                WithOne(a => a.user).
                HasForeignKey(a => a.userId).
                OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>().
              HasMany<Product_Like>(s => s.productLikes).
              WithOne(a => a.user).
              HasForeignKey(a => a.userId).
              OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>().
               HasMany<Blog>(s => s.blogs).
               WithOne(a => a.user).
               HasForeignKey(a => a.userId).
               OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>().
               HasMany<Order>(s => s.orders).
               WithOne(a => a.user).
               HasForeignKey(a => a.userId).
               OnDelete(DeleteBehavior.Restrict);

            // Order
            modelBuilder.Entity<Order>().
              HasMany<OrderDetail>(s => s.orderDetails).
              WithOne(a => a.order).
              HasForeignKey(a => a.orderId).
              OnDelete(DeleteBehavior.Restrict);

            // Product
            modelBuilder.Entity<Product>().
              HasMany<OrderDetail>(s => s.orderDetails).
              WithOne(a => a.product).
              HasForeignKey(a => a.productId).
              OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Product>().
              HasMany<Rating>(s => s.ratings).
              WithOne(a => a.product).
              HasForeignKey(a => a.productId).
              OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Product>().
              HasMany<Product_Like>(s => s.productLikes).
              WithOne(a => a.product).
              HasForeignKey(a => a.productId).
              OnDelete(DeleteBehavior.Restrict);

            // Role
            modelBuilder.Entity<Role>().
              HasMany<User>(s => s.users).
              WithOne(a => a.role).
              HasForeignKey(a => a.roleId).
              OnDelete(DeleteBehavior.Restrict);
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Category> Categorys { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Product_Like> Product_Likes { get; set; }
    }
}
