﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace farmfresh_c.Models
{

    [Table("Category")]
    public class Category : BaseModel
    {

        public string title { get; set; }
        public string slug { get; set; }
        public bool showToHomePage { get; set; }
        public virtual ICollection<Product> products { get; set; }

        public Category()
        {

        }



    }
}
