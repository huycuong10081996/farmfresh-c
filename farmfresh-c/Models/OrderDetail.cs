﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace farmfresh_c.Models {
    [Table("OrderDetail")]
    public class OrderDetail {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int quantity { get; set; }
        public Order order { get; set; }
        public Product product { get; set; }
        public int productId { get; set; }
        public int orderId { get; set; }
        public OrderDetail() {
        }

        public OrderDetail(int quantity, int productId, int orderId) {
            this.quantity = quantity;
            this.productId = productId;
            this.orderId = orderId;
        }

        public OrderDetail(int id, int quantity, int productId, int orderId) {
            this.id = id;
            this.quantity = quantity;
            this.productId = productId;
            this.orderId = orderId;

        }
    }
}
