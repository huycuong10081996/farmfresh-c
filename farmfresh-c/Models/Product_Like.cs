﻿using System.ComponentModel.DataAnnotations.Schema;

namespace farmfresh_c.Models {
    [Table("Product_Like")]
    public class Product_Like : BaseModel {

        public User user { get; set; }
        public int userId { get; set; }
        public Product product { get; set; }
        public int productId { get; set; }
        public Product_Like() {
        }
    }
}
