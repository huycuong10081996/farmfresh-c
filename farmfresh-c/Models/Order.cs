﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace farmfresh_c.Models
{
    [Table("Order")]
    public class Order : BaseModel
    {
        public string district { get; set; }
        public string address { get; set; }
        public string province { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phone { get; set; }
        public DateTime deliveryDate { get; set; }
        public int status { get; set; }
        public double totalMoney { get; set; }
        public User user { get; set; }
        public int userId { get; set; }
        public virtual ICollection<OrderDetail> orderDetails { get; set; }



    }
}
