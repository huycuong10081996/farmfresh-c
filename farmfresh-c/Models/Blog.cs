﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace farmfresh_c.Models {
    [Table("Blog")]
    public class Blog : BaseModel {
        public string title { get; set; }
        public string content { get; set; }
        public string image { get; set; }
        public bool? status { get; set; }
        public string slug { get; set; }
        public virtual ICollection<Rating> ratings { get; set; }
        public User user { get; set; }
        public int userId { get; set; }


    }
}
