﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace farmfresh_c.Models
{
    [Table("Product")]
    public class Product : BaseModel
    {
        public string name { get; set; }
        public string imageUrl { get; set; }
        public int? status { get; set; }
        public string type { get; set; }
        public double price { get; set; }
        public double? discount { get; set; }
        public string description { get; set; }
        public int quantity { get; set; }
        public string color { get; set; }
        public Category category { get; set; }
        public int categoryId { get; set; }
        public virtual ICollection<Rating> ratings { get; set; }
        public virtual ICollection<OrderDetail> orderDetails { get; set; }
        public virtual ICollection<Product_Like> productLikes { get; set; }

    }
}
