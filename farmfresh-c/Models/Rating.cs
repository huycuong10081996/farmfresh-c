﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace farmfresh_c.Models {
    [Table("Rating")]
    public class Rating : BaseModel {

        public string ratingFor { get; set; }
        public string description { get; set; }
        public int rating { get; set; }
        public Product product { get; set; }
        public Blog blog { get; set; }
        public User user { get; set; }
        public int? blogId { get; set; }
        public int userId { get; set; }
        public int? productId { get; set; }
        public Rating() {
        }


    }
}
