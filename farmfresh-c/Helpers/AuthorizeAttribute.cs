﻿using System;
using farmfresh_c.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace farmfresh_c.Helpers {
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter {
        public void OnAuthorization(AuthorizationFilterContext context) {
            var user = (User)context.HttpContext.Items["User"];
            if (user == null) {
                // not logged in
                context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
            if (user.status == 1) {
                context.Result = new JsonResult(
                    new { message = "Your account has been locked by admin cause of " + user.rejectReason }) { StatusCode = StatusCodes.Status403Forbidden };
            }
        }
    }
}
