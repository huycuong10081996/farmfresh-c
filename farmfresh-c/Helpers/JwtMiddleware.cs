﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using farmfresh_c.Services;

namespace farmfresh_c.Helpers {
    public class JwtMiddleware {
        private readonly RequestDelegate _next;
        private readonly string SECRET_TOKEN = "RANDOM_TOKEN_SECRET";

        public JwtMiddleware(RequestDelegate next) {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IUserService userService) {
            var authorToken = context.Request.Headers["Authorization"];
            var typeAuthor = authorToken.FirstOrDefault()?.Split(" ").First();
            var token = authorToken.FirstOrDefault()?.Split(" ").Last();

            if (token != null)
                attachUserToContext(context, userService, token, typeAuthor);

            await _next(context);
        }

        private void attachUserToContext(HttpContext context, IUserService userService, string token, string typeAuthor) {
            try {
                // attach user to context on successful jwt validation
                if (typeAuthor == "Bearer") {
                    context.Items["User"] = userService.GetUserByAccessToken(token);
                } else if (typeAuthor == "Basic") {
                    context.Items["User"] = userService.GetUserByBasicAuthor(token);
                }
            } catch {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }
}
