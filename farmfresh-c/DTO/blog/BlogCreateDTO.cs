﻿using System;
namespace farmfresh_c.DTO {
    public class BlogCreateDTO {
        public int id { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public string image { get; set; }
        public bool? status { get; set; }
        public string slug { get; set; }
        public int userId { get; set; }

        public BlogCreateDTO() {
        }

        public BlogCreateDTO(string title, string content, string image, bool? status, string slug, int userId) {
            this.title = title;
            this.content = content;
            this.image = image;
            this.status = status;
            this.slug = slug;
            this.userId = userId;
        }
    }
}
