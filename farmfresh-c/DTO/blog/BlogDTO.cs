﻿using System;
using System.Collections.Generic;
using farmfresh_c.Models;

namespace farmfresh_c.DTO {
    public class BlogDTO : BaseGetAllDTO<Blog> {

        public BlogDTO() {
        }

        public BlogDTO(ICollection<Blog> Data, int? Total, int? Limit, int? Skip) {
            data = Data;
            total = Total;
            limit = Limit;
            skip = Skip;
        }
    }
}
