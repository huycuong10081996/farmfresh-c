﻿using System;
using farmfresh_c.Models;

namespace farmfresh_c.DTO {
    public class AuthenticateResponse {
        public bool success;
        public Authenticate data;
        public AuthenticateResponse() {
        }

        public AuthenticateResponse(bool success, Authenticate data) {
            this.success = success;
            this.data = data;
        }
    }
}
