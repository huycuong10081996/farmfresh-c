﻿using System;
namespace farmfresh_c.DTO {
    public class UserCreateDTO {
        public string email { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string name { get; set; }

        public UserCreateDTO() {
        }

        public UserCreateDTO(string email, string password) {
            this.email = email;
            this.password = password;
        }

        public UserCreateDTO(string email, string name, string password, string firstName, string lastName, string phone, string address, string district, string province) : this(email, password) {
            this.firstName = firstName;
            this.name = name;
            this.lastName = lastName;
            this.phone = phone;
            this.address = address;
            this.district = district;
            this.province = province;
        }
    }
}
