﻿using System;
using System.Collections.Generic;
using farmfresh_c.Models;

namespace farmfresh_c.DTO {
    public class UserDTO : BaseGetAllDTO<User> {

        public UserDTO() {

        }

        public UserDTO(ICollection<User> Data, int? Total, int? Limit, int? Skip) {
            data = Data;
            total = Total;
            limit = Limit;
            skip = Skip;
        }

    }
}
