﻿using System;
namespace farmfresh_c.DTO.user {
    public class UserUpdateForAdminDTO {
        public string email { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string name { get; set; }
        public int? status { get; set; }
        public int roleId { get; set; }
        public string rejectReason { get; set; }

        public UserUpdateForAdminDTO() {
        }

        public UserUpdateForAdminDTO(string email, string password) {
            this.email = email;
            this.password = password;
        }

        public UserUpdateForAdminDTO(string email, string name, string password, int? status, string firstName, string lastName, string phone, string address, string district, string province, int roleId, string rejectReason) : this(email, password) {
            this.firstName = firstName;
            this.name = name;
            this.lastName = lastName;
            this.phone = phone;
            this.address = address;
            this.district = district;
            this.province = province;
            this.roleId = roleId;
            this.status = status;
            this.rejectReason = rejectReason;
        }
    }
}


