﻿using System;
using System.Collections.Generic;
using farmfresh_c.Models;

namespace farmfresh_c.DTO {
    public class CategoryDTO : BaseGetAllDTO<Category> {

        public CategoryDTO() {

        }

        public CategoryDTO(ICollection<Category> Data, int? Total, int? Limit, int? Skip) {
            data = Data;
            total = Total;
            limit = Limit;
            skip = Skip;
        }
    }
}
