﻿using System;
namespace farmfresh_c.DTO {
    public class CategoryCreateDTO {
        public int id { get; set; }
        public string title { get; set; }
        public bool? showToHomePage { get; set; }
        public string slug { get; set; }
        public CategoryCreateDTO() {
        }

        public CategoryCreateDTO(string title, bool? showToHomePage, string slug) {
            this.title = title;
            this.showToHomePage = showToHomePage;
            this.slug = slug;
        }
    }
}
