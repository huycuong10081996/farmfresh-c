﻿using System;
namespace farmfresh_c.DTO {
    public class RatingCreateDTO {
        public int? productId { get; set; }
        public int? userId { get; set; }
        public int? blogId { get; set; }
        public int? rating { get; set; }
        public string description { get; set; }

        public RatingCreateDTO() {
        }

        public RatingCreateDTO(int? productId, int? userId, int? blogId, int? rating, string description) {
            this.productId = productId;
            this.userId = userId;
            this.blogId = blogId;
            this.rating = rating;
            this.description = description;
        }
    }
}
