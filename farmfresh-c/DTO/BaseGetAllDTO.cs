﻿using System;
using System.Collections.Generic;
using farmfresh_c.Models;

namespace farmfresh_c.DTO {
    public class BaseGetAllDTO<T> {

        public virtual ICollection<T> data { get; set; }
        public int? total { get; set; }
        public int? limit { get; set; }
        public int? skip { get; set; }

        public BaseGetAllDTO() {

        }

        public BaseGetAllDTO(ICollection<T> Data, int? Total, int? Limit, int? Skip) {
            data = Data;
            total = Total;
            limit = Limit;
            skip = Skip;
        }
    }
}
