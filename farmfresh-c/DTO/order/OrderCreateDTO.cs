using System;
using System.Collections.Generic;
using farmfresh_c.Models;
namespace farmfresh_c.DTO {

    public class OrderCreateDto {

        public string address { get; set; }
        public string district { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string province { get; set; }
        public string phone { get; set; }
        public double? totalAmount { get; set; }
        public int? userId { get; set; }
        public DateTime deliveryDate { get; set; }
        public int? status;
        public List<OrderDetailCreateDto> products { get; set; }
        public OrderCreateDto() {
        }

        public OrderCreateDto(string address, string district, string lastName, string firstName, string province, string phone, double? totalAmount, int? userId, DateTime deliveryDate, int? status, List<OrderDetailCreateDto> products) {
            this.address = address;
            this.district = district;
            this.lastName = lastName;
            this.firstName = firstName;
            this.province = province;
            this.phone = phone;
            this.totalAmount = totalAmount;
            this.userId = userId;
            this.deliveryDate = deliveryDate;
            this.status = status;
            this.products = products;
        }
    }
}