using System;
using System.Collections.Generic;
using farmfresh_c.Models;

namespace farmfresh_c.DTO {
    public class OrderDTO : BaseGetAllDTO<Order> {

        public OrderDTO() {
        }

        public OrderDTO(ICollection<Order> Data, int? Total, int? Limit, int? Skip) {
            data = Data;
            total = Total;
            limit = Limit;
            skip = Skip;
        }
    }
}
