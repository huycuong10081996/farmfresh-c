using farmfresh_c.Models;
namespace farmfresh_c.DTO
{

    public class OrderDetailCreateDto
    {
        public Product product { get; set; }
        public int? quantity { get; set; }
        public OrderDetailCreateDto()
        {
        }

        public OrderDetailCreateDto(Product product, int? quantity)
        {
            this.product = product;
            this.quantity = quantity;
        }
    }
}