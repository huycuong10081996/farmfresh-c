﻿using System;
using System.Collections.Generic;
using farmfresh_c.Models;

namespace farmfresh_c.DTO {
    public class ProductDTO : BaseGetAllDTO<Product> {

        public ProductDTO() {
        }

        public ProductDTO(ICollection<Product> Data, int? Total, int? Limit, int? Skip) {
            data = Data;
            total = Total;
            limit = Limit;
            skip = Skip;
        }
    }
}
