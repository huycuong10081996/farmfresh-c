﻿using System;
namespace farmfresh_c.DTO {
    public class ProductCreateDTO {
        public int? id { get; set; }
        public int? categoryId { get; set; }
        public string name { get; set; }
        public double? price { get; set; }
        public double? discount { get; set; }
        public int? quantity { get; set; }
        public string description { get; set; }
        public string color { get; set; }
        public string imageUrl { get; set; }
        public int? status { get; set; }
        public string type { get; set; }

        public ProductCreateDTO() {
        }
    }
}
